<?php

namespace App\Application;
use App\Models\User;
use App\Infrastructure\Repositories\EloquentGenericRepository;
use App\Providers\AppServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AuthService{
  public function login(string $email,string $password){
        try {
          if(Auth::attempt(['email' =>$email, 'password' =>$password ])){
            $user = User::find(Auth::user()->id);
            $tokenResult = $user->createToken('api_facturacion');
            $token =$tokenResult->token;
            $data = [
              'access_token' => $tokenResult->accessToken,
              'token_type' => 'Bearer',
              'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
            ];

            return $data;
       }
         }catch (\Exception $e) {
            throw $e;
        }
    }
}