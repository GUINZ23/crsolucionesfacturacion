<?php

namespace App\Application;
use App\Models\Barrios; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;


class BarriosService{

    protected $barrioService;// esta variable va a contener la referencia del la instancia del modelo Barrios

    /**
   *  Se agrega contructor donde se relealiza una inyeccion del modelo que va a utlilzar y se realiza una nueva instacia de la clase 
   * EloquentGenericRepository para poder acceder a los metodos definidos en ellos 
   * LA clase EloquentGenericRepository es una clase generica que procesa las consultas a apartir del 
   * del modelo que se le paso por el constructor.
   */

    public function __construct(Barrios $barrios) {
        $this->barrioService = new EloquentGenericRepository($barrios);
    }
     /*
    Clase del servicio que devuelve los barrios  en base a los paramatros de entrada 
    */
    public function getBarrios(int $id_provincia,int $id_canton,int $id_distrito,string $name_colum_provincia,string $name_colum_canton,string $name_colum_distrito){
        try {
            return  $this->barrioService->where3Params($id_provincia,$id_canton,$id_distrito,$name_colum_provincia,$name_colum_canton,$name_colum_distrito);
        } catch (\Exeption $ex ) {
            throw $ex ;
        }
    }
}