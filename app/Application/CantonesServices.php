<?php

namespace App\Application;
use App\Models\Cantones; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;


class CantonesServices 
{
   protected $cantonesRespository; // esta variable va a contener la referencia del la instancia del mdelo Cantones

  /**
   *  Se agrega contructor donde se relealiza una inyeccion del modelo que va a utlilzar y se realiza una nueva instacia de la clase 
   * EloquentGenericRepository para poder acceder a los metodos definidos en ellos 
   * LA clase EloquentGenericRepository es una clase generica que procesa las consultas a apartir del 
   * del modelo que se le paso por el constructor.
   */
  public function __construct(Cantones $cantones) {
       $this->cantonesRespository = new EloquentGenericRepository($cantones) ;
  }
  /*
    Clase del servicio que devuelve los cantones  en base a los paramatros de entrada 
  */
  public function getCantones($id_provincia,$nombre_columna){ 
     try {
        return $this->cantonesRespository->where($id_provincia,$nombre_columna);
     } catch (\Exception $e) {
           throw $e;
     }
  }
}
