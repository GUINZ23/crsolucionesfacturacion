<?php
namespace App\Application;
use App\Models\Certificado; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;
use Illuminate\Support\Facades\Storage;
use Exception;
class CertificadoService  {
    
     protected $certificadoRepository;

    public function __construct(Certificado $certificadoRepository) {
     $this->certificadoRepository = new EloquentGenericRepository($certificadoRepository);

    }

    public function createCertificado($request){
        try {
            if ($request->has('archivo_p12_base64')) {
                $contenidoBase64 = $request->input('archivo_p12_base64');
                $contenidoBinario = base64_decode($contenidoBase64);
                $nombreArchivo = $request->input('numero_identificacion').'.p12';
                if (Storage::disk('public')->exists($nombreArchivo)) {
                    throw new Exception('Un archivo con este nombre ya existe.');
                }
                if(Storage::disk('public')->put($nombreArchivo, $contenidoBinario)){
                return $this->certificadoRepository->create([
                    'usuario_certificado'=>$request->input('usuario_certificado'),
                    'contrasena_certificado'=>$request->input('contrasena_certificado'),
                    'ruta_certificado'=>$nombreArchivo,
                    'companies_id'=>$request->input('companies_id')
                  ]);
                }
            }
        }catch (\Exception $e) {
            throw $e;
    
        }
      
    }
}