<?php

namespace App\Application;

use App\Models\CodigoPais; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;;

class CodigoPaisService {

    protected $codigoPaisRepository;
    /**
     * Class constructor.
     */
    public function __construct(CodigoPais $codigoPais)
    {
        $this->codigoPaisRepository =new EloquentGenericRepository($codigoPais);
    }
    public function getAllCodigoPais()
    {
        try {
            return $this->codigoPaisRepository->getAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}