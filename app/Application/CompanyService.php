<?php

namespace App\Application;

use App\Models\Company; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;

class CompanyService
{
    protected $companyRepository;

    public function __construct( Company $company)
    {
        $this->companyRepository =  new EloquentGenericRepository($company);
    }

    public function getAllCompanies()
    {
        try {
            return $this->companyRepository->getAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function createCompany(array $company)
    {
        try {
            $result = $this->companyRepository->create($company);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateCompany(int $companyId, array $data)
    {
        try {
            $result = $this->companyRepository->update($companyId, $data);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteCompany(int $companyId)
    {
        try {
            $result = $this->companyRepository->delete($companyId);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}