<?php

namespace App\Application;

use App\Models\Distrito; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;

class DistritoService {
   protected $distritoRespository; // instacia del del modelo Distrito

   /**
    * contructor que  tiene un inyecion de dependcia del modelo al cual se referencia esto para la clase EloquentGenericRepository
    */
  public function __construct(Distrito $distrito) {
     $this->distritoRespository = new EloquentGenericRepository($distrito);
  }
  /**
   *   Esta funcion recibe como parametros los valores para realizar una busqueda con los parametros requeridoss para la consulta de los distritos 
   * en base a la provicia y la cantones , se le pasan las variables de los nombres de las columas donde debe buscar 
   */
 public  function getDistritos(int $provincias_id,int $cantones_id,string $nombre_columna_provincia,string $nombre_columna_canton)
  {
     try {
          return $this->distritoRespository->where2Params($provincias_id,$cantones_id,$nombre_columna_provincia,$nombre_columna_canton);
     } catch (\Exception $ex ) {
         throw  $ex;
     }
  } 
}