<?php

namespace App\Application;
use App\Models\Provincias; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;

class ProvinciasService{
    protected $provinciasRepository; 

    public function __construct(Provincias $provincias) {
        $this->provinciasRepository = new EloquentGenericRepository($provincias);
    }
    public function getAllProvincias()
    {
        try {
            return $this->provinciasRepository->getAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}