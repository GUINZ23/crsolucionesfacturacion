<?php

namespace App\Application;

use App\Models\TiposCedulas; // Asegúrate de tener el modelo correcto
use App\Infrastructure\Repositories\EloquentGenericRepository;


class TiposCedulasService{
    protected $tiposCedulasRepository;

    public function __construct(TiposCedulas $tiposCedulas)
    {
        $this->tiposCedulasRepository =new EloquentGenericRepository($tiposCedulas);
    }

    public function getAllTiposCedulas()
    {
        try {
            return $this->tiposCedulasRepository->getAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function createTiposCedulas(string $name, string $email)
    {
        try {
            $result = $this->tiposCedulasRepository->create(['name' => $name, 'email' => $email]);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateTiposCedulas(int $companyId, array $data)
    {
        try {
            $result = $this->tiposCedulasRepository->update($companyId, $data);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteTiposCedulas(int $companyId)
    {
        try {
            $result = $this->tiposCedulasRepository->delete($companyId);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}