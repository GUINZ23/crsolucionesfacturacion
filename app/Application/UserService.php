<?php

namespace App\Application;

use App\Models\User;
use App\Infrastructure\Repositories\EloquentGenericRepository;

class UserService
{
    protected $userRepository;

    public function __construct(User $user)
    {
        $this->userRepository = new EloquentGenericRepository($user);
    }

    public function getAllUsers()
    {
        try {
            $result = $this->userRepository->getAll();
            return $result;
        } catch (\Exception $e) {
            throw $e;
    
        }
    }
    public function createUser( string $numero_identificacion, string $razon_social, int $tipos_cedulas_id, string $email,string $password)
    {
       
        try {
            $result = $this->userRepository->create(['numero_identificacion' => $numero_identificacion,
             'razon_social' => $razon_social,'email'=>$email,'tipos_cedulas_id'=>$tipos_cedulas_id,'password'=>$password]);

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateUser(int $userId, array $data)
    {
        try {
            $result = $this->userRepository->update($userId, $data);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteUser(int $userId)
    {
        try {
            $result = $this->userRepository->delete($userId);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}