<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\AuthService;
use App\Http\Responses\ApiResponse;


class AuthController extends Controller
{
    protected $authService;
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function login(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');
       
        try {
            $result = $this->authService->login($email, $password);
            return ApiResponse::success($result, 'Login success.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }
}
