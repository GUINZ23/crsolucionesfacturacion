<?php

namespace App\Http\Controllers;

use App\Models\Barrios;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use App\Application\BarriosService;
use App\Http\Requests\BarrioRequest;

class BarriosController extends Controller
{
   protected $barriosService;

   public function __construct(BarriosService $barriosService) {
    $this->barriosService = $barriosService;
   }     
     public  function getBarrios(BarrioRequest $request){
         try {
            $id_provincia = $request->input('id_provincia');
            $nombre_columna_provincia= $request->input('nombre_columna_provincia');
            $cantones_id= $request->input('cantones_id');
            $nombre_columna_canton=$request->input('nombre_columna_canton'); 
            $distritos_id= $request->input('distritos_id');
            $nombre_columna_distritos=$request->input('nombre_columna_distrito'); 
            $barrios = $this->barriosService->getBarrios($id_provincia,$cantones_id,$distritos_id,$nombre_columna_provincia,$nombre_columna_canton,$nombre_columna_distritos);
            return ApiResponse::success($barrios,"Barrios retrieved successfully.");
         } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
     }
}
