<?php

namespace App\Http\Controllers;

use App\Models\Cantones;
use Illuminate\Http\Request;
use App\Application\CantonesServices;
use App\Http\Responses\ApiResponse;
use App\Http\Requests\CantonesRequest;

class CantonesController extends Controller
{
  protected $cantonesService;
  public function __construct(CantonesServices $cantonesService) {
   $this->cantonesService = $cantonesService;
  }

  public function getCantones (CantonesRequest $request){
        $id_provincia = $request->input('id_provincia');
        $nombre_columna= $request->input('nombre_columna');
        try {
            $cantones =  $this->cantonesService->getCantones($id_provincia,$nombre_columna);
           return ApiResponse::success($cantones,"Cantones retrieved successfully.");
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
      
  }
}
