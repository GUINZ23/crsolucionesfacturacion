<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CertificadoRequest;
use App\Application\CertificadoService;
use App\Http\Responses\ApiResponse;

class CertificadoController extends Controller
{
    protected $certificadoService;
     public function __construct(CertificadoService $certificadoService) {
      $this->certificadoService = $certificadoService;
   }

   public function createCertificado(CertificadoRequest $request){
   
    try {
         $data= $request;
         $result = $this->certificadoService->createCertificado($data);
        return ApiResponse::success($result, 'Certificado created successfully.');
    } catch (\Exception $e) {
        return ApiResponse::error($e->getMessage());
    }
   }
}
