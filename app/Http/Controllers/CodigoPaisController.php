<?php

namespace App\Http\Controllers;

use App\Models\CodigoPais;
use Illuminate\Http\Request;
use App\Application\CodigoPaisService; // Asegúrate de tener el servicio correcto
use App\Http\Responses\ApiResponse;

class CodigoPaisController extends Controller
{
    protected  $codigoPaisService;

    public function __construct(CodigoPaisService $codigoPaisService) {
        $this->codigoPaisService = $codigoPaisService;
    }

    public function getAllCodigoPais()
    {
        $codigoPais = $this->codigoPaisService->getAllCodigoPais();

        return ApiResponse::success($codigoPais, 'codigoPais retrieved successfully.');
    }
}
