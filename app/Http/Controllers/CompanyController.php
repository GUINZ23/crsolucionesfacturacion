<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\CompanyService; // Asegúrate de tener el servicio correcto
use App\Http\Responses\ApiResponse;

class CompanyController extends Controller
{
    protected $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function getAllCompanies()
    {
        $companies = $this->companyService->getAllCompanies();

        return ApiResponse::success($companies, 'Companies retrieved successfully.');
    }

    public function createCompany(Request $request)
    {
         $company= $request->all();
        try {
             $result = $this->companyService->createCompany($company);
            return ApiResponse::success($result, 'Company created successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }

    public function updateCompany(Request $request, int $companyId)
    {
        $data = $request->all();
        // dd($companyId);die;
        try {
            $result = $this->companyService->updateCompany($companyId, $data);
            return ApiResponse::success($result, 'Company updated successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }

    public function deleteCompany(int $companyId)
    {
        try {
            $result = $this->companyService->deleteCompany($companyId);
            return ApiResponse::success($result, 'Company deleted successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }
}