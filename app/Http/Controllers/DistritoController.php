<?php

namespace App\Http\Controllers;

use App\Models\Distrito;
use Illuminate\Http\Request;
use App\Application\DistritoService; // Asegúrate de tener el servicio correcto
use App\Http\Responses\ApiResponse;
use App\Http\Requests\DistritoRequest;

class DistritoController extends Controller
{
  protected $distritoService;

   public function __construct(DistritoService $distritoService) {
    $this->distritoService = $distritoService;
   }

   public function getDistrito (DistritoRequest $request){
    $id_provincia = $request->input('id_provincia');
    $nombre_columna_provincia= $request->input('nombre_columna_provincia');
    $cantones_id= $request->input('cantones_id');
    $nombre_columna_canton=$request->input('nombre_columna_canton');
    try {
        $distritos =  $this->distritoService->getDistritos($id_provincia,$cantones_id,$nombre_columna_provincia,$nombre_columna_canton);
       return ApiResponse::success($distritos,"Distritos retrieved successfully.");
    } catch (\Exception $e) {
        return ApiResponse::error($e->getMessage());
    }
  
}
}
