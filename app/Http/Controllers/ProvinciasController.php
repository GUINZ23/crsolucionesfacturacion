<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\ProvinciasService; // Asegúrate de tener el servicio correcto
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Http;

class ProvinciasController extends Controller
{
    protected $provinciasService;

    public function __construct(ProvinciasService $provinciasService)
    {
        $this->provinciasService = $provinciasService;
    }

    public function getAllProvincias()
    {
        $provincias = $this->provinciasService->getAllProvincias();

        return ApiResponse::success($provincias, 'Provincias  retrieved successfully.');
    }
public function convertM3UToJson()
{
    $url = 'https://iptv-org.github.io/iptv/index.m3u';
    $response = Http::get($url);

    if ($response->successful()) {
        $contenido = $response->body();
        $lineas = explode("\n", $contenido);
        
        $canales = [];

        foreach ($lineas as $linea) {
            $linea = trim($linea);
            if (strpos($linea, '#EXTINF:') === 0) {
                // Extraer metadatos de la línea EXTINF
                $metadata = $this->extraerMetadataDeExtinf($linea);
            } elseif (!empty($linea) && $linea[0] !== '#') {
                // Asumir que esta línea es la URL del stream
                $canales[] = array_merge($metadata, ['url' => $linea]);
            }
        }

        return response()->json($canales);
    } else {
        return response()->json(['error' => 'No se pudo acceder al archivo M3U.'], 400);
    }
}

    private function extraerMetadataDeExtinf($lineaExtinf)
{
    // Extraer la parte de metadatos después de "#EXTINF:-1 "
    $parteMetadatos = substr($lineaExtinf, strpos($lineaExtinf, ' ')+1);
    $metadatos = [];

    // Extraer tvg-id
    preg_match('/tvg-id="([^"]*)"/', $parteMetadatos, $tvgId);
    $metadatos['tvg_id'] = $tvgId[1] ?? null;

    // Extraer tvg-logo
    preg_match('/tvg-logo="([^"]*)"/', $parteMetadatos, $tvgLogo);
    $metadatos['tvg_logo'] = $tvgLogo[1] ?? null;

    // Extraer group-title
    preg_match('/group-title="([^"]*)"/', $parteMetadatos, $groupTitle);
    $metadatos['group_title'] = $groupTitle[1] ?? null;

    // Extraer el nombre del canal (después de la última coma)
    $nombreCanal = trim(substr($parteMetadatos, strrpos($parteMetadatos, ',')+1));
    $metadatos['nombre_canal'] = $nombreCanal;

    return $metadatos;
}
   
}
