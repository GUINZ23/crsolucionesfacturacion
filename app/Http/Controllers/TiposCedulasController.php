<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\TiposCedulasService; // Asegúrate de tener el servicio correcto
use App\Http\Responses\ApiResponse;

class TiposCedulasController extends Controller
{
    protected $tiposCedulasService;

    public function __construct(TiposCedulasService $tiposCedulasService)
    {
        $this->tiposCedulasService = $tiposCedulasService;
    }

    public function getAllTiposCedulas()
    {
        $companies = $this->tiposCedulasService->getAllTiposCedulas();

        return ApiResponse::success($companies, 'Tipo Cedulas retrieved successfully.');
    }
}
