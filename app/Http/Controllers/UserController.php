<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\UserService;
use App\Http\Responses\ApiResponse;
use App\Http\Requests\CreateUserRequest;
class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getAllUsers()
    {
        $users = $this->userService->getAllUsers();

        return ApiResponse::success($users, 'Users retrieved successfully.');
    }
    public function createUser(CreateUserRequest $request)
    {
        $numero_identificacion = $request->input('numero_identificacion');
        $razon_social = $request->input('razon_social');
        $tipos_cedulas_id=$request->input('tipos_cedulas_id');
        $email=$request->input('email');
        $password=$request->input('password');
        try {
            $result = $this->userService->createUser($numero_identificacion, $razon_social,$tipos_cedulas_id,$email,$password);
            return ApiResponse::success($result, 'User created successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }

    public function updateUser(Request $request, int $userId)
    {
        $data = $request->all();

        try {
            $result = $this->userService->updateUser($userId, $data);
            return ApiResponse::success($result, 'User updated successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }

    public function deleteUser(int $userId)
    {
        try {
            $result = $this->userService->deleteUser($userId);
            return ApiResponse::success($result, 'User deleted successfully.');
        } catch (\Exception $e) {
            return ApiResponse::error($e->getMessage());
        }
    }
}