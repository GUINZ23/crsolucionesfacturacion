<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class BarrioRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id_provincia' => 'required',
            'nombre_columna_provincia'=>'required',
            'cantones_id'=>'required',
            'nombre_columna_canton'=>'required',
            'distritos_id'=>'required',
            'nombre_columna_distrito'=>'required'
            // Agrega más reglas según tus necesidades
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, response()->json([
            'error' => 'Validation failed',
            'details' => $validator->errors(),
        ], 422));
    }
}