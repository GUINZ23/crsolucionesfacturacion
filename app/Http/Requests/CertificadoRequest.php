<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Rules\Base64FormatRule;

class CertificadoRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'archivo_p12_base64' => ['required', new Base64FormatRule],
             'numero_identificacion'=>'required'
            // Agrega más reglas según tus necesidades
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, response()->json([
            'error' => 'Validation failed',
            'details' => $validator->errors(),
        ], 422));
    }
}