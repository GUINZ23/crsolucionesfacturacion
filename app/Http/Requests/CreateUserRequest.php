<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
class CreateUserRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'numero_identificacion' => 'required|string|max:255',
            'razon_social' =>'required|string|max:255',
            'email' => 'required|email|unique:cuentas|max:255',
            'tipos_cedulas_id'=>'required|integer|max:1|min:1',
            'password' => 'required|min:6',
            // Agrega más reglas según tus necesidades
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, response()->json([
            'error' => 'Validation failed',
            'details' => $validator->errors(),
        ], 422));
    }
}