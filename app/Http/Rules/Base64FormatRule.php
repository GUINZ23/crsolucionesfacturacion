<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class Base64FormatRule implements Rule
{
    public function passes($attribute, $value)
    {
        // Verifica si el valor es una cadena y si puede ser decodificado correctamente
        $result = is_string($value) && base64_encode(base64_decode($value)) === $value;
       // dd($result);die;
        return $result;
    }

    public function message()
    {
        return 'El campo :attribute no está en formato base64 válido.';
    }
}