<?php
namespace App\Infrastructure\Repositories;

use Illuminate\Database\Eloquent\Model;

class EloquentGenericRepository implements GenericRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        try {
            return $this->model->all()->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getById($id)
    {
        try {
            return $this->model->find($id)->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function create(array $data)
    {
        try {
            return $this->model->create($data)->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($id, array $data)
    {
        try {
            $record = $this->model->find($id);
            $record->update($data);

            return $record->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);
            return ['success' => $result];
        } catch (\Exception $e) {
            throw $e;
        }
    }
    public function where(int $id,string $name_colum){
        try {
           $result=$this->model->where($name_colum,$id)->get();
           return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    public function where2Params(int $id,int $id2,string $name_colum,$name_colum2){
        try {
           $result=$this->model->where($name_colum,$id)->where($name_colum2,$id2)->get();
           return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    public function where3Params(int $id,$id2,$id3,$name_colum,$name_colum2,$name_colum3){
          try {
             $result = $this->model->where($name_colum,$id)->where($name_colum2,$id2)->where($name_colum3,$id3)->get();
              return $result;
          } catch (\Exception $e ) {
             throw  $e;
          }
    }
}