<?php
namespace App\Infrastructure\Repositories;
interface GenericRepository
{
    public function getAll();
    public function getById($id);
    public function create(array $data);
    public function update($id, array $data);
    public function delete($id);
    public function where(int $id,string $name_colum);// se agrega una funcion personalizada para buscar en una tabla atravez de un where
}