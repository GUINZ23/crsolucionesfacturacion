<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cantones extends Model
{
    use HasFactory;
    protected $table = 'cantones';
    protected $fillable = [
        'nombre_columna',
        'id_provincia'
    ];
}
