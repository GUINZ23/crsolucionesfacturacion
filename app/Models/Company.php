<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $table = 'companies';
    protected $fillable = [
        'nombre_compania',
        'nombre_cormercial',
        'numero_identificacion',
        'correo',
        'telefono',
        'celular',
        'dirrecion',
        'dirrecion_web',
        'logo',
        'codigo_pais_id',
        'tipos_cedulas_id',
        'provincias_id',
        'cantones_id',
        'distritos_id',
        'barrios_id'
    ];
}
