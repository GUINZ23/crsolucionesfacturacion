<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('barrios', function (Blueprint $table) {
            $table->unsignedBigInteger('distritos_id');
            $table->foreign('distritos_id')
            ->references('codigo_distritos_id')
            ->on('distritos')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('barrios', function (Blueprint $table) {
            Schema::dropIfExists('barrios');
        });
    }
};
