<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('barrios', function (Blueprint $table) {
            $table->unsignedBigInteger('provincias_id');
            $table->unsignedBigInteger('cantones_id');

            $table->foreign('provincias_id')
            ->references('codigo_provincia_id')
            ->on('provincias')
            ->onDelete('cascade');

            
            $table->foreign('cantones_id')
            ->references('codigo_cantones_id')
            ->on('cantones')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('barrios', function (Blueprint $table) {
            Schema::dropIfExists('barrios');
        });
    }
};
