<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_compania',80);
            $table->string('nombre_cormercial',100);
            $table->string('numero_identificacion',20);
            $table->string('correo',160)->unique();
            $table->integer("telefono")->length(20);
            $table->integer("celular")->nullable()->length(20);
            $table->string('logo')->nullable();
            $table->unsignedBigInteger('tipos_cedulas_id');
            $table->unsignedBigInteger('codigo_pais_id');
            $table->unsignedBigInteger('provincias_id');
            $table->unsignedBigInteger('cantones_id');
            $table->unsignedBigInteger('distritos_id');
            $table->unsignedBigInteger('barrios_id');
            $table->string('dirrecion',160);
            $table->string('dirrecion_web')->nullable();


            $table->foreign('barrios_id')
            ->references('id')
            ->on('barrios')
            ->onDelete('cascade');


            $table->foreign('provincias_id')
            ->references('id')
            ->on('provincias')
            ->onDelete('cascade');

            
            $table->foreign('cantones_id')
            ->references('id')
            ->on('cantones')
            ->onDelete('cascade');
            
            $table->foreign('distritos_id')
            ->references('id')
            ->on('distritos')
            ->onDelete('cascade');

            $table->foreign('codigo_pais_id')
            ->references('id')
            ->on('codigo_pais')
            ->onDelete('cascade');

            $table->foreign('tipos_cedulas_id')
            ->references('id')
            ->on('tipos_cedulas')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
