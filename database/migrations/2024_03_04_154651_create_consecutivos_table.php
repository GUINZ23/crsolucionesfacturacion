<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('consecutivos', function (Blueprint $table) {
            $table->id();
            $table->string("fe_cosecutivo")->comment('Numero consecutivo Factura electronica');
            $table->string("te_cosecutivo")->comment('Numero consecutivo Tiquete electronico');
            $table->string("fc_cosecutivo")->comment('Numero consecutivo  Factura de Compra');
            $table->string("fex_cosecutivo")->comment('Numero consecutivo Factura de Exportacion');
            $table->string("proforma_cosecutivo")->comment('Numero consecutivo Proforma');
            $table->string("nc_cosecutivo")->comment('Numero consecutivo Nota Credito');
            $table->string("nd_cosecutivo")->comment('Numero consecutivo Nota Debito');
            $table->unsignedBigInteger('companies_id');
            $table->foreign('companies_id')
            ->references('id')
            ->on('companies')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('consecutivos');
    }
};
