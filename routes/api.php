<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\TiposCedulasController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CodigoPaisController;
use App\Http\Controllers\ProvinciasController;
use App\Http\Controllers\CantonesController;
use App\Http\Controllers\DistritoController;
use App\Http\Controllers\BarriosController;
use App\Http\Controllers\CertificadoController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'users'], function () {
    Route::post('/', [UserController::class, 'createUser']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/', [UserController::class, 'getAllUsers']);
    Route::get('/{id}', [UserController::class, 'getUserById']);
    Route::put('/{id}', [UserController::class, 'updateUser']);
    Route::delete('/{id}', [UserController::class, 'deleteUser']);
});
Route::group(['prefix' => 'companies'], function () {
    Route::post('/createCompany', [CompanycreateCompanyController::class, 'createCompany']);
    Route::get('/', [CompanyController::class, 'getAllCompanies']);
    Route::patch('/updateCompany/{id}', [CompanyController::class, 'updateCompany']);
    Route::delete('/{id}', [CompanyController::class, 'deleteCompany']);
});

Route::group(['prefix' => 'tipos_cedulas'], function () {
    Route::post('/', [TiposCedulasController::class, 'createTiposCedulas']);
    Route::get('/', [TiposCedulasController::class, 'getAllTiposCedulas']);
    Route::put('/{id}', [TiposCedulasController::class, 'updateTiposCedulas']);
    Route::delete('/{id}', [TiposCedulasController::class, 'deleteTiposCedulas']);
});

Route::group(['prefix' => 'v1/autenticacion'], function () {
    Route::post('/login', [AuthController::class, 'login']);
});

Route::group(['prefix' => 'codigoPais'], function () {
    Route::get('/', [CodigoPaisController::class, 'getAllCodigoPais']);
});

Route::group(['prefix' => 'provincias'], function () {
    Route::get('/', [ProvinciasController::class, 'getAllProvincias']);
    Route::get('/canales', [ProvinciasController::class, 'convertM3UToJson']);
});
Route::group(['prefix' => 'cantones'], function () {
    Route::post('/getCantones', [CantonesController::class, 'getCantones']);
});
Route::group(['prefix' => 'distritos'], function () {
    Route::post('/getDistrito', [DistritoController::class, 'getDistrito']);
});
Route::group(['prefix' => 'barrios'], function () {
    Route::post('/getBarrios', [BarriosController::class, 'getBarrios']);
});
Route::group(['prefix' => 'certificados'], function () {
    Route::post('/createCertificado', [CertificadoController::class, 'createCertificado']);
});
