CREATE TRIGGER add_consecutive_to_companie
AFTER INSERT
ON companies FOR EACH ROW
BEGIN
    INSERT INTO consecutivos (fe_cosecutivo, te_cosecutivo, fc_cosecutivo, fex_cosecutivo, proforma_cosecutivo, nc_cosecutivo, nd_cosecutivo, companies_id) 
    VALUES ('1', '1', '1', '1', '1', '1', '1', NEW.id);
END;